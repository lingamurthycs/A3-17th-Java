package com.hcl.a3hack.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hcl.a3hack.domain.Checkin;
import com.hcl.a3hack.domain.OfferDetail;
import com.hcl.a3hack.service.OfferService;



@Controller
@RequestMapping(value = "/a3hack")
public class OfferController {

	private Logger logger = Logger.getLogger(OfferController.class);
	
	@Autowired
	OfferService offerService;
	
	@RequestMapping(value="/checkin", method=RequestMethod.POST, produces="application/json")
	public void userCheckin(@RequestBody Checkin checkin){
		logger.info(checkin);
		offerService.userCheckin(checkin);
	}
	
	@RequestMapping(value="/offers", method=RequestMethod.GET)
	@ResponseBody
	public List<OfferDetail> getOffers(){
		return new ArrayList<OfferDetail>();
	}
	
	@RequestMapping(value="/offer/{offerId}", method=RequestMethod.GET)
	@ResponseBody
	public OfferDetail getOfferDetail(@PathVariable("offerId") String offerId){
		
		return new OfferDetail();
	}
}
	

