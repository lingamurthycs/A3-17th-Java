package com.hcl.a3hack.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OfferDetail {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long offerId;
	
	private String cif;
	
	private String deviceId;
	
	private String deviceLocationLat;

	private String deviceLocationLon;
	
	private Date timePlaced;
	
	private String placeId;
	
	private String placeName;
	
	private String placeAddress;
	
	private Date generatedTime;
	
	private Date expirateTime;
	
	private String state;
	
	private String geofenceId;

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceLocationLat() {
		return deviceLocationLat;
	}

	public void setDeviceLocationLat(String deviceLocationLat) {
		this.deviceLocationLat = deviceLocationLat;
	}

	public String getDeviceLocationLon() {
		return deviceLocationLon;
	}

	public void setDeviceLocationLon(String deviceLocationLon) {
		this.deviceLocationLon = deviceLocationLon;
	}

	public Date getTimePlaced() {
		return timePlaced;
	}

	public void setTimePlaced(Date timePlaced) {
		this.timePlaced = timePlaced;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPlaceAddress() {
		return placeAddress;
	}

	public void setPlaceAddress(String placeAddress) {
		this.placeAddress = placeAddress;
	}

	public Date getGeneratedTime() {
		return generatedTime;
	}

	public void setGeneratedTime(Date generatedTime) {
		this.generatedTime = generatedTime;
	}

	public Date getExpirateTime() {
		return expirateTime;
	}

	public void setExpirateTime(Date expirateTime) {
		this.expirateTime = expirateTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getGeofenceId() {
		return geofenceId;
	}

	public void setGeofenceId(String geofenceId) {
		this.geofenceId = geofenceId;
	}
	
	
	
}
