package com.hcl.a3hack.domain;

import java.util.Date;

public class Checkin {

	private String cif;
	
	private String deviceId;
	
	private String deviceLocationLat;

	private String deviceLocationLon;
	
	private Date timePlaced;
	
	private String geofenceId;
	
	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceLocationLat() {
		return deviceLocationLat;
	}

	public void setDeviceLocationLat(String deviceLocationLat) {
		this.deviceLocationLat = deviceLocationLat;
	}

	public String getDeviceLocationLon() {
		return deviceLocationLon;
	}

	public void setDeviceLocationLon(String deviceLocationLon) {
		this.deviceLocationLon = deviceLocationLon;
	}

	public Date getTimePlaced() {
		return timePlaced;
	}

	public void setTimePlaced(Date timePlaced) {
		this.timePlaced = timePlaced;
	}

	public String getGeofenceId() {
		return geofenceId;
	}

	public void setGeofenceId(String geofenceId) {
		this.geofenceId = geofenceId;
	}
}
