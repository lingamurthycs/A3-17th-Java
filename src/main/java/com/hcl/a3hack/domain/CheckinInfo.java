package com.hcl.a3hack.domain;

import java.util.Date;

public class CheckinInfo {

	private int pingCount;
	
	private Date startTime;
	
	private String deviceId;
	
	private String geofenceId;

	public int getPingCount() {
		return pingCount;
	}

	public void setPingCount(int pingCount) {
		this.pingCount = pingCount;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getGeofenceId() {
		return geofenceId;
	}

	public void setGeofenceId(String geofenceId) {
		this.geofenceId = geofenceId;
	}
	
	
}
