package com.hcl.a3hack.domain;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.util.Key;

public class GooglePlacesURL extends GenericUrl {

    public GooglePlacesURL(String encodedUrl) {
      super(encodedUrl);
    }
    
    @Key
    public String fields;
}
