package com.hcl.a3hack.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.hcl.a3hack.domain.GooglePlacesURL;
import com.hcl.a3hack.domain.OfferDetail;

public class GoogleApiService {
	
	Logger log = Logger.getLogger(GoogleApiService.class);
	
	static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	static final JsonFactory JSON_FACTORY = new JacksonFactory();
	
	private static void run(OfferDetail offerDetail) throws Exception {
	    HttpRequestFactory requestFactory =
	        HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
	            @Override
	          public void initialize(HttpRequest request) {
	            request.setParser(new JsonObjectParser(JSON_FACTORY));
	          }
	        });
	    GooglePlacesURL url = new GooglePlacesURL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyC2ijVpQJuh9JUhwPk_ceDChQxecPcqBgk&name=starbucks&location=-33.87325200%2C151.20867600&radius=500&type=cafe");
	    url.fields = "id,name,place_id,vicinity";
	    HttpRequest request = requestFactory.buildGetRequest(url);
	    GooglePlaces places = request.execute().parseAs(GooglePlaces.class);
	    if (places.list.isEmpty()) {
	    	Iterator<Place> listOfPlaces = places.list.iterator();
	    	for(int i=0;i<places.list.size();i++){
	    		if(places.list.get(i).name=="Starbucks"){
	    			offerDetail.setPlaceAddress(places.list.get(i).vicinity);
	    			offerDetail.setPlaceName(places.list.get(i).name);
	    			
	    		}
	    		
	    	}
	    } else {
	      if (places.hasMore) {
	      }
	   
	    }
	  }
	
	
	 public static class GooglePlaces {
		    @Key
		    public List<Place> list;

		    @Key("has_more")
		    public boolean hasMore;
		  }
	 
	public static class Place {
	    @Key
	    public String id;

//	    @Key
//	    public List<String> tags;

	    @Key
	    public String name;

//	    @Key
//	    public String url;
//	    
	    @Key
	    public String place_id;
	    
	    @Key
	    public String vicinity;
	    
	  }

}
