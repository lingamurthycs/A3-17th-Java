package com.hcl.a3hack.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.a3hack.domain.Checkin;
import com.hcl.a3hack.domain.OfferDetail;
import com.hcl.a3hack.repositories.OfferRepository;
import com.hcl.a3hack.service.OfferService;

@Service
public class OfferServiceImpl implements OfferService {

	
	private OfferRepository offerRepository;	
	
	public OfferServiceImpl(@Autowired OfferRepository offerRepository){
		this.offerRepository = offerRepository;
	}
	private Map<String, ArrayList<Checkin>> checkinMap = new HashMap<String, ArrayList<Checkin>>();
	
	@Override
	public String userCheckin(Checkin checkin) {
		if(isValidUserCheckin(checkin)){
			offerRepository.save(createDummy(checkin));
		}
		return null;
	}
	
	private boolean isValidUserCheckin(Checkin checkin){
		String key = checkin.getCif()+"-"+checkin.getGeofenceId();
		ArrayList<Checkin> checkins = checkinMap.get(key);
		
		if(checkins != null ) {
			if(checkins.size() == 2)
				return true;
			else 
				checkins.add(checkin);
		}
		else {
			checkins = new ArrayList<Checkin>();
			checkins.add(checkin);
			checkinMap.put(key, checkins);
		}
		return false;
	}
	
	private void createOffer(Checkin checkin){
		OfferDetail offerDetail = new OfferDetail();
		//offerRepository.save(createDummy());
	}
	
	
	
	
	private OfferDetail createDummy(Checkin checkin){
		OfferDetail offerDetail = new OfferDetail();
		Date exTime = new GregorianCalendar().getTime();
	
		offerDetail.setCif(checkin.getCif());
		offerDetail.setDeviceId(checkin.getDeviceId());
		offerDetail.setDeviceLocationLat(checkin.getDeviceLocationLat());
		offerDetail.setDeviceLocationLon(checkin.getDeviceLocationLon());
		offerDetail.setGeofenceId(checkin.getGeofenceId());
		//offerDetail.setExpirateTime();
		return offerDetail;
	}


}