package com.hcl.a3hack.service;

import org.springframework.stereotype.Service;

import com.hcl.a3hack.domain.Checkin;
import com.hcl.a3hack.repositories.OfferRepository;


public interface OfferService {

	public String userCheckin(Checkin checkin);
	
}
