package com.hcl.a3hack.dao;

import java.util.List;

import com.hcl.a3hack.domain.OfferDetail;

public interface OfferDao {

	OfferDetail save(OfferDetail offerDetail);
	
	List<OfferDetail> getOffers();
	
	OfferDetail getOfferDetail(int offerId);
	
}
