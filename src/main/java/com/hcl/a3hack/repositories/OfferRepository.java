package com.hcl.a3hack.repositories;

import org.springframework.data.repository.CrudRepository;

import com.hcl.a3hack.domain.OfferDetail;

public interface OfferRepository extends CrudRepository<OfferDetail, Long>{

}
